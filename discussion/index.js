// Comments

/* 
    Two types of comments
    1. Single Line
    2. Multi Line
*/

console.log("Hello World");

// Variables
/* 
    -it is used to contain data 
*/

//Declaring Variables - tells our devises that a variable name is created  and is ready to store data

/* 
    Syntax:
        let/const variableName;

        let - keyword usually used in declaring a variable
        const - keyword usually used in declaring CONSTANT variable
*/
let myVariable;

//console.log(myVariable);

//console.log(hello);

/* Initializing variables - the instance when a variable is given it's initial/starting value. */

/* Syntax:
        let/const variableName = variable;
*/

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

//Reassigning variable values
// Syntax: variableName = newValue;

productName = "Laptop";
console.log(productName);

//interest = 4.489;

/* Declares a variable first */
let supplier;

// Initialization of value
supplier = "John Smith Tradings";

/* Multiple variable declaration */

let productCode = "DC017",
  productBrand = "Dell";

console.log(productCode, productBrand);

//Data types

// String - handles a series of characters that create a word, phrase, or a sentence.

let country = "Philippines";
let province = "Metro Manila";

// Concatenating strings (+ is used)
let fullAddress = province + " " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

/* Escape character (\) in strings is combination with other characters can produce a different effect (\n) */

let mailAddress = "Metro Manila\n\nPhilippines";

console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

//Numbers
//Integers/Whole numbers

let headcount = 32;
console.log(headcount);

/* Decimal numbers/fractions */
let grade = 98.7;
console.log(grade);

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

/* Combining text/numbers and strings */
console.log("John's grade last quarter is " + grade);

//Boolean -used to store values relating to the state of certain things

let isMarried = true;
let inGoodConduct = false;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

/* Array - are special kind of data type tha'ts used to store multiple values; */

/* Syntax: let/const arrayName = [elementA, elementB, elementC, ...] */
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Objects - another special kind of data type that's used to mimic real world objects/items

/* 
    Syntax:
    let/const objectName = {
        propertyA: value,
        propertyB: value,
    }

*/

let person = {
  fullname: "Juan Dela Cruz",
  age: 35,
  isMarried: false,
  contact: ["0917 123 4567", "8123 4567"],
  address: {
    houseNumber: "345",
    city: "Manila",
  },
};

console.log(person);

let myGrades = {
  firstGrade: 98.7,
  secondGrading: 92.1,
  thirdGrading: 90.2,
  fourthGrading: 94.6,
};

console.log(myGrades);

//typeof operator - used to determing the type of data or the value of the variable.

console.log(typeof myGrades);

console.log(typeof grades);

// Null -used to intentionally express the absence of a value in a variable declaration/initialization.

let spouse = null;
let myNumber = 0;
let myString = "";

// Undefined
let fullName;
console.log(fullName);
